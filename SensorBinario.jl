### A Pluto.jl notebook ###
# v0.19.40

using Markdown
using InteractiveUtils

# ╔═╡ f01f0420-fb3f-11ee-1599-6def20b9d2d3
using PlutoUI, Plots, PrettyTables

# ╔═╡ 3dcbcdb3-0971-4bd3-8a9b-251ccef4dcc6
md"""

$\text{RELATÓRIO}$ 
$\textbf{Testes de utilização do SENSOR de BINÁRIO}$
$\textbf{Ensaios em carga da bancada N.º 8}$
---
"""

# ╔═╡ 5672df31-2195-45e8-be61-f84469e758c5
text"""
Ensaio: Ricardo Luís / Paulo Almeida
Data: 15·04·2024

Relatório: Ricardo Luís
Notebook: SensorBinario.jl 
Last update: 17·04·2024

ISEL, Lab.ME 
"""

# ╔═╡ 9abde5bc-f8ac-4d0d-ba9a-82e09eb8d6b0


# ╔═╡ ece18487-c3b1-426e-8765-9999a8f1fac0
md"""
# Introdução
"""

# ╔═╡ 15f9a593-452e-4c37-9eb0-166a6727a875
md"""
O presente relatório apresenta os resultados de utilização do sensor de binário de ``\pm 50\rm Nm``, instalado na bancada n.º 8: 

Fabricante: ``\rm Burster``\
Modelo: ``\text{8656-5050-v01020}``\
download: [datasheet](https://www.burster.com/fileadmin/user_upload/redaktion/Documents/Products/Data-Sheets/Section_8/8656_EN.pdf)  \
Saída analógica: ``\pm 10\rm V``\
Tensão de alimentação: ``\text{de 10 a 30V (DC)}``

O sensor de binário foi testado nos ensaios em carga do motor de indução de rotor bobinado e do motor série. Como carga mecânica destes motores foi utilizado o gerador de corrente contínua alimentando carga resistiva.

 ![](https://gitlab.com/labme/notebooks/-/raw/main/images/bancad8.jpg?ref_type=heads)
"""

# ╔═╡ 829eb1fb-25e4-4508-b674-5fd842b0a7db


# ╔═╡ 401f5b75-c2b8-44c7-a89b-e8dc8610a915
md"""
## Máquinas elétricas
"""

# ╔═╡ 503555a3-c445-4047-a065-dac940755acb
md"""
### Motor de indução 3~ de rotor bobinado
"""

# ╔═╡ 70966e56-3cf9-4785-98f8-98ed052c964d
md"""
Motor de indução: ``\text{n.º 952-A}``\
Specs: $$\:\text{ 2.2kW} \quad cos \varphi=0.67 \quad \rm 50 Hz$$\
 $$\quad \quad \quad \rm 220/380V\quad \Delta/Y$$\
 $$\quad \quad \quad \rm 11.3/6.5A\quad \Delta/Y$$\
 $$\quad \quad \quad \rm rotor:\: 100V\:; \: 14A\quad Y$$\
 $$\quad \quad \quad n_n=1390\rm rpm$$
"""

# ╔═╡ 69fe9f61-6b63-4386-9f22-5d07485b897d
# Binário nominal (Nm)
Tₘₙ = 2200/(2π*1390/60)

# ╔═╡ 69fd01ff-848d-497e-a9c3-4ee8257b59f3
# Medição resistências do motor de indução: rotórica (Ω), estatórica (Ω/fase)
R₂₂, R₁ = 0.72, 2.24; 

# ╔═╡ e0bc73f5-847c-4987-99cc-f1094cfc86ba
# Ensaio de rotor bloqueado: (W), (A), (V)
Pcc, Icc, Ucc = 699.9, 6.50, 93.0;

# ╔═╡ 2c88e77d-430f-4a91-828d-2087c3657842


# ╔═╡ 54d36fb9-f5fd-4b0e-91ed-e3468bf503e4
md"""
### Motor série
"""

# ╔═╡ 1ddb9524-1ffa-45e8-9e13-5e4c858de917
md"""
Motor série: ``\text{n.º 958}``\
Specs: $$\:\text{ 220V, 10.6A; 1.9kW  @ 1500rpm;\quad}$$\
 $$\quad\quad \quad n_{max}=2500\text{rpm}$$ 
"""

# ╔═╡ e5eb112c-4b35-49a1-84e0-be90201728de
# Binário nominal (Nm)
Tₘₛₙ = 1900/(2π*1500/60)

# ╔═╡ 552314d1-08da-4374-9281-cbab23505917
# Medição resistência do motor série [ induzido + indutor ] (Ω)
Rₛₑᵣᵢₑ = 3.69;

# ╔═╡ d7146b57-78f8-4abd-9df3-bc73e4fc5ce0


# ╔═╡ 24c470e9-5b20-4d1e-906c-d6e6eaab867d
md"""
### Gerador DC (carga)
"""

# ╔═╡ 81f9941a-9011-48b0-9237-7d8e6c6dfae5
md"""
Gerador DC com indutor shunt/independente: ``\text{n.º 941}``\
Specs: $$\text{ 250V, 16A; 4kW  @ 1500rpm;\quad} I_{exc}=0.57\text{A}$$ 
"""

# ╔═╡ 2e89f897-bf0b-4756-8c9b-a37c37d68943
# Medição resistência do gerador, induzido, (Ω)
Rᵢᵍᵉʳ = 2.0;

# ╔═╡ 9a4b4736-038c-4167-8b8d-41beb2cfbd50


# ╔═╡ bd3f5136-789a-4ffe-bfe1-473846e2accb
md"""
# Ensaios em carga
"""

# ╔═╡ 4ce12df2-4b03-4e3a-9802-6decb834a648
md"""
## Motor de indução
"""

# ╔═╡ 23bcdd74-0ed8-4cf2-94b7-26f262069de4
md"""
O gerador DC foi utilizado em excitação derivação.\
Realizou-se a medição de temperatura do motor, inicial (após período de aquecimento em carga) e final do ensaio: $\quad \rm T_i=42°C\quad; \quad T_f=71°C\quad$ com a câmara termográfica.
"""

# ╔═╡ 6dea539c-3a5c-4b35-a38d-842b4d945172
md"""
Dados do ensaio em carga:
"""

# ╔═╡ c08064f0-c2a1-492d-8baf-70337f494cf2
# Tensão do motor indução 3~ (V)
Uₘ = [380.5, 380.9, 380.1, 380.8, 382.1, 380.9, 381.4, 381.4, 381.0, 380.2, 379.5, 383.0, 380.1, 380.3, 380.4, 380.4, 379.4, 380.2];

# ╔═╡ 0c957459-d62c-4816-baf3-d5b7e45d8621
# Corrente do motor indução 3~ (A)
Iₘ = [5.21, 5.27, 5.23, 5.30, 5.55, 5.60, 5.67, 5.80, 5.89, 6.28, 6.56, 6.75, 6.76, 6.91, 7.65, 7.84, 7.92, 9.05];

# ╔═╡ 37c737d2-0ea7-4dff-ba25-f0bde0cc66fd
# Potência absorvida do motor indução 3~ (W)
P = [619.2, 842.5, 1009, 1177, 1406, 1654, 1795, 1953, 2064, 2484, 2763, 2881, 2985, 3086, 3749, 3880, 3958, 4855];

# ╔═╡ c345cd74-6678-4f88-ad3f-e3e802e415b6
# Binário do motor indução 3~ , saída analógica (V) 
Tₘᵥ = [0.15, 0.44, 0.65, 0.85, 1.18, 1.49, 1.67, 1.86, 2.01, 2.50, 2.82, 2.95, 3.08, 3.20, 3.93, 4.07, 4.18, 5.16];

# ╔═╡ a7e920ae-1cc8-4957-9ae1-8d2107b5fe79
# Binário do motor indução 3~  (Nm)
Tₘ = 5* Tₘᵥ;

# ╔═╡ e1aacc9f-6870-4dc6-9b5f-c3fe4bef7095
# Velocidade (rpm)
nₘ = [1483, 1474, 1468, 1461, 1451, 1442, 1436, 1429, 1424, 1407, 1396, 1391, 1384, 1380, 1349, 1343, 1335, 1285];

# ╔═╡ d4a7905a-8afc-4ee4-8953-bfbd675c3a36
# Tensão do gerador (V)
Uₗ₁ = [NaN, 200, 196, 192, 204, 205, 199, 196, 191, 202, 202, 197, 192, 188, 200, 196, 191, 203];

# ╔═╡ 223f5994-4681-4182-8802-b571cc2d7c7f
# Corrente de carga do gerador (A)
Iₗ₁ = [NaN, 0.78, 1.58, 2.35, 3.10, 4.1, 4.8, 5.5, 6.2, 7.4, 8.3, 8.9, 9.5, 10.0, 11.5, 12.0, 12.5, 14.1];

# ╔═╡ 57fe55aa-3fd1-4ae9-b95b-24050af6e593
# Corrente de excitação do gerador (mA)
Iₑₓ₁ = [NaN, 285, 279, 275, 316, 317, 308, 303, 296, 337, 338, 327, 321, 313, 384, 377, 368, 460];

# ╔═╡ 486405b4-6715-42ff-bbaa-adee8fbc2cf6


# ╔═╡ e2f8a679-1514-4aaa-a02c-2c45f28f5066
md"""
### Tabela
"""

# ╔═╡ d3b15d47-380b-47d9-98b0-bad0a6636bd5
md"""
> **Nota:** a 1ª linha corresponde ao motor em vazio. Daqui são extraídos dados para o circuito equivalente do motor e as perdas rotacionais do gerador.
"""

# ╔═╡ ae03db7b-f1d3-4f3b-9297-787cb8b3dcff
header_mi = (["Tensão motor",  "Corrente motor", "Potência absorvida", "Binário", "Velocidade", "Tensão ger.", "Corrente ger.", "Corrente exc. ger."], 						 ["(V)", "(A)", "(W)", "(Nm)", "(rpm)", "(V)", "(A)", "(mA)"]);

# ╔═╡ 653f7651-ed88-43ba-a18e-dbd3561e4edf
dados_mi = [Uₘ Iₘ P Tₘ nₘ Uₗ₁ Iₗ₁ Iₑₓ₁];

# ╔═╡ 73eb930f-d8d4-4c0a-bced-24463139d1f3
pretty_table(HTML, dados_mi, header = header_mi)

# ╔═╡ 802e98da-35c1-4363-92bb-06b7878aedf5
md"""
## Motor série
"""

# ╔═╡ c7057229-f0df-4988-b2e1-e665c1b925ef
md"""
O gerador DC foi utilizado em excitação separada.
"""

# ╔═╡ 0b6cc374-1131-475e-a0da-9a9862f795df
md"""
Dados do ensaio em carga:
"""

# ╔═╡ e1d6f54f-e982-459c-814b-239382ea9885
# Tensão do motor série (V)
Uₘₛ = [220, 220, 220, 220, 220, 220, 219, 219, 219, 219, 219, 220, 221, 221, 220, 220];

# ╔═╡ 3432e497-037e-4fbe-b5ec-6eab0e51bbf7
# Corrente do motor série (A)
Iₘₛ = [16.6, 16.1, 15.7, 15.3, 14.4, 13.1, 12.0, 10.7, 9.7, 8.0, 7.0, 6.5, 6.0, 4.9, 4.4, 3.9];

# ╔═╡ a5db8e95-a16c-44d2-b489-de67edde07f9
# Binário do motor série, saída analógica (V) 
Tₘₛᵥ = [4.37, 4.24, 4.11, 3.98, 3.72, 3.28, 2.94, 2.49, 2.21, 1.70, 1.44, 1.27, 1.11, 0.82, 0.68, 0.55];

# ╔═╡ 1e362a43-7544-4b9a-9bc9-e3f5a7d3d9bd
# Binário do motor série (Nm)
Tₘₛ = 5 * Tₘₛᵥ;

# ╔═╡ 3938b3c2-5a95-40ba-9af9-e46299ca88bd
# Velocidade (rpm)
nₘₛ = [1218, 1225, 1237, 1254, 1284, 1347, 1403, 1498, 1552, 1697, 1799, 1880, 1978, 2200, 2348, 2538];

# ╔═╡ c544d93a-735a-4ed2-a831-57eae3fd7b6b
# Tensão do gerador (V)
Uₗ₂ = [167, 170, 173, 177, 185, 199, 211, 212, 202, 226, 212, 221, 210, 235, 216, 234];

# ╔═╡ e57911ef-94fc-44ba-99d1-d2c9205f7a3e
# Corrente de carga do gerador (A)
Iₗ₂ = [13.9, 13.4, 12.9, 12.5, 11.5, 10.0, 8.8, 7.9, 7.5, 5.6, 5.3, 4.6, 4.4, 3.0, 2.8, 2.0];

# ╔═╡ 91a83b0f-675c-4c37-8d6c-12bcb73be459
# Corrente de excitação do gerador (mA)
Iₑₓ₂ = [378, 378, 378, 378, 378, 378, 376, 330, 286, 286, 236, 236, 202, 201, 164, 163];

# ╔═╡ d81e65a0-ab51-4230-8f08-9c5f438c52c7


# ╔═╡ f28ded18-f411-4a9e-8a3b-7893305d7456
md"""
### Tabela
"""

# ╔═╡ fbac4c2c-eee4-4e0b-a758-f31afb4d203d
header_ms = (["Tensão motor",  "Corrente motor", "Binário", "Velocidade", "Tensão ger.", "Corrente ger.", "Corrente exc. ger."], 						 ["(V)", "(A)", "(Nm)", "(rpm)", "(V)", "(A)", "(mA)"]);

# ╔═╡ 90ea3a5c-69ee-4d61-a82e-9b5e16431224
dados_ms = [Uₘₛ Iₘₛ Tₘₛ nₘₛ Uₗ₂ Iₗ₂ Iₑₓ₂];

# ╔═╡ b86a6a50-70ca-4a48-b8bb-35409bf5c7fe
pretty_table(HTML, dados_ms, header = header_ms)

# ╔═╡ 7464554b-7efc-41a0-b6f5-75fa94b3498b
md"""
# Análise de resultados
"""

# ╔═╡ bec4aee7-6be4-4520-933e-853c97f94a1b
md"""
## Motor de indução
"""

# ╔═╡ 4a448fcb-45e2-4311-a54b-c0b1db31ac8d
md"""
### Método indireto
"""

# ╔═╡ 5bdaf528-518b-4d52-b7ed-b9bfa170b6bd
Pd₁ᵍᵉʳ = Uₗ₁ .* Iₗ₁ .+ Rᵢᵍᵉʳ * (Iₗ₁ .+ Iₑₓ₁/1000).^2 + Uₗ₁ .* Iₑₓ₁/1000;

# ╔═╡ c4b0f227-6c95-4176-82a5-4358c461a861
Pab₁ᵍᵉʳ = Pd₁ᵍᵉʳ .+ Tₘ[1]*(2π*nₘ[1]/60)

# ╔═╡ d7dd42d5-9920-4908-ada4-0294fc1635e7
Tₘ₁ = Pab₁ᵍᵉʳ ./ (2π*nₘ/60)

# ╔═╡ 30002726-91c2-4867-93c3-eb7c61d898a6


# ╔═╡ 6fa13a2f-cfcd-4175-8a7c-3cd567cfb4a7
md"""
### Cálculo binário desenvolvido
"""

# ╔═╡ 95ce86af-8d24-4155-a385-34f017d2c841


# ╔═╡ a5b09778-a1ad-4765-a112-6b277f752265
md"""
Parâmetros do circuito equivalente aproximado:
"""

# ╔═╡ a2d8de84-6577-43e6-b9d3-22f08af4a9b1
cos_φcc = Pcc/(√3 * Ucc * Icc)

# ╔═╡ 1b8694da-3cf8-49a4-8206-8129fa7e5b83
Req = (Ucc/√3) * cos_φcc / Icc

# ╔═╡ 0588d273-7a6a-46a0-b0e0-78c13f8fe688
R₂ = Req - R₁

# ╔═╡ 8b64d9d1-8bac-4a9a-bfca-0d7d9c8b02eb
md"""
Validação de R₂:
"""

# ╔═╡ 02cef2c8-1708-413e-b9df-ab6a9c15aacf
R₂ᵥ= (R₂₂/2) * (380/100)^2

# ╔═╡ 4611bd79-684b-4084-8db4-99f6b7f18b9d


# ╔═╡ 2dc02479-da9e-4825-9e79-06eeafeb33ff
md"""
Corrente de vazio, $$\overline{I_0}$$:
"""

# ╔═╡ 13a7c180-03f5-4e6d-ba22-30f3279b57a2
I₀ = Iₘ[1]

# ╔═╡ 1630ffce-a1ce-4f4c-9671-1c5269c2a945
cos_φ₀ = P[1]/(√3 * Uₘ[1] * Iₘ[1])

# ╔═╡ 43ae126c-138d-4944-a3c4-307793572ebf
begin
	φ₀ = acos(cos_φ₀)
	φ₀= rad2deg(φ₀)
end

# ╔═╡ 7f2a7dcf-3658-4228-a49f-b4e267a3a5ab


# ╔═╡ a0d28c87-76c1-4ebf-8b2f-f533763059e5
md"""
Correntes rotóricas, $$\overline{I_r}$$:
"""

# ╔═╡ 76e4ffb3-e084-4bcf-aca3-61f73b615c6e
cos_φ = P ./ (√3 * Uₘ .* Iₘ)

# ╔═╡ 303c31f7-a3d2-4286-888c-9c0678810581
begin
	φ = acos.(cos_φ)
	rad2deg.(φ)
end

# ╔═╡ 48d42242-6be5-4ce1-bed8-e31a699f6e32


# ╔═╡ d7c6448e-88e4-4296-a7fc-6422a99fe4b0
md"""
Binário desenvolvido:
"""

# ╔═╡ 11dc37d1-3e99-47fa-931f-8acef83c4d7a
s = (1500 .- nₘ) / 1500

# ╔═╡ a7f2e529-8a96-4042-893d-5f7ee3158697


# ╔═╡ f5a1f575-06e7-4b94-bd7c-451e3f743023
md"""
### Resultados (motor de indução) 📈
"""

# ╔═╡ ffe768b3-28bd-40d7-b112-b1b488c6c557


# ╔═╡ ef57fa7c-e953-4c52-ac1d-bee152414b66
md"""
## Motor série
"""

# ╔═╡ 5aedc763-0808-49a6-a6d5-3ade37390400
md"""
### Método indireto
"""

# ╔═╡ ae356ba7-3182-4cbc-b671-7c5ef9a20433
Pd₂ᵍᵉʳ  = Uₗ₂ .* Iₗ₂ .+ Rᵢᵍᵉʳ * Iₗ₂.^2;

# ╔═╡ fbfd7d67-40da-455f-b091-47cb7fb6fda8
Pab₂ᵍᵉʳ = Pd₂ᵍᵉʳ .+ Tₘ[1]*(2π*nₘ[1]/60) .* (nₘₛ / nₘ[1])

# ╔═╡ e73c62aa-1937-4771-984f-f5125ffd4a28
Tₘ₂ = Pab₂ᵍᵉʳ ./ (2π*nₘₛ/60)

# ╔═╡ 53f9620a-1e7a-4c66-9f7e-5256bb6d023b


# ╔═╡ 94d1beda-944c-4d6a-9538-a0169c863a1b
md"""
### Cálculo binário desenvolvido
"""

# ╔═╡ 19c1324f-d0ee-48ad-ab1a-23ebaa38f27a
fcem = Uₘₛ .-  Rₛₑᵣᵢₑ * Iₘₛ;

# ╔═╡ a949170a-f7f3-4c5e-8b1b-55909353ee6a
Td₂ = (fcem ./ (2π*nₘₛ/60)) .* Iₘₛ

# ╔═╡ 20b25bc6-aa15-4432-ba15-3120d0b09f40


# ╔═╡ 66e768ba-3dc9-4127-913e-ccea0a2d2fea
md"""
### Resultados (motor série) 📈
"""

# ╔═╡ 7be786d5-8dc7-4813-8493-775ff012bc17
begin
	scatter(Iₘₛ, Tₘₛ, label= "lab (sensor de binário)", 
					title="Característica de binário", 
					xlabel="corrente (A)", ylabel="binário (Nm)")
	plot!(Iₘₛ, Tₘₛ, lc=:blue, lw= 2, label=:none)
	plot!(Iₘₛ, Tₘ₂, lc=:red, lw=2, label = "Binário (método indireto)")
	plot!(Iₘₛ, Td₂, lc=:green, lw=2, line=:dash, label = "Binário desenvolvido")
	scatter!([10.6], [Tₘₛₙ], label="nominal", mc=:purple)
end

# ╔═╡ a028e3bf-6076-4c52-a4ef-71227e9873b4
begin
	scatter(Iₘₛ, nₘₛ, label= "lab", 
					title="Característica de velocidade", 
					xlabel="corrente (A)", ylabel="velocidade (rpm)")
	plot!(Iₘₛ, nₘₛ, lc=:blue, lw=2; label=:none)
	scatter!([10.6], [1500], label="nominal", mc=:purple)
end

# ╔═╡ e75e437c-e885-47e3-b24c-3a5118f9d014


# ╔═╡ 9bd3af5e-79ba-4956-a140-d0be852489f1
md"""
# Conclusões
"""

# ╔═╡ fbdfb92f-709e-40b7-ba0b-5b50d46f57c3
begin
	Tm = filter(!=(first(Tₘ)),Tₘ)
	Tm1 = filter(!isnan,Tₘ₁)
	ϵ₁ = (Tm .- Tm1) *100 ./ Tm
	ϵ₁ = abs.(ϵ₁)
	ϵ₁ᵐᵃˣ = round(maximum(ϵ₁), digits=1)
end

# ╔═╡ 1e9a79f9-6e58-4e1d-8f97-56f74d971f4e
begin
	ϵ₂ = (Tₘₛ - Tₘ₂) *100 ./ Tₘₛ
	ϵ₂ = abs.(ϵ₂)
	ϵ₂ᵐᵃˣ = round(maximum(ϵ₂), digits=1)
end

# ╔═╡ b5f1ae37-7d99-42aa-93d8-157a9e79a81b
begin
	Tₘᵐᵃˣ = maximum(Tₘ)
	Kₜ = (maximum(Tₘ) - Tₘₙ)*100/Tₘₙ
	Kₜ = round(Kₜ, digits=1)
end

# ╔═╡ 77c3f2bc-ce4f-4173-b8b1-bace74bcc6ad
md"""
As curvas experimentais de ambos os motores testados em carga, com os dados fornecidos pelo sensor de binário, contêm praticamente os respetivos valores nominais, o que é um bom indicador.

Foram determinados os binários desenvolvidos de ambos os motores utilizando os respetivos modelos de regime permanente. Seria expetável que as respetivas curvas dos motores ensaiados utilizando os valores de binário desenvolvido estivessem sempre acima das curvas experimentais com os dados de binário mecânico do sensor de binário, expondo assim as perdas rotacionais de cada um dos motores testados.

Comparando os resultados obtidos pelo sensor de binário com os obtidos pelo método indireto (calculando o binário pelo lado do gerador DC) verifica-se uma boa validação com erro relativos máximos de $(ϵ₁ᵐᵃˣ)% no ensaio em carga do motor de indução e de $(ϵ₂ᵐᵃˣ)% no ensaio em carga do motor série. Neste último, é de salientar que as perdas rotacionais do gerador DC foram consideradas proporcionais à variação de velocidade, para o cálculo do método indireto do motor série, dada a sua ampla variação de velocidade. 

No ensaio em carga do motor de indução de rotor bobinado foi também observado o comportamento térmico do motor, visto ter sido removido o ventilador para permitir o acoplamento entre as máquinas e sensor de binário. Emulou-se a situação de o motor ser utilizado já com alguma temperatura de funcionamento, como se tivesse sido utilizado numa aula prática anterior. Assim, do início para o fim do ensaio a temperatura na carcaça do motor variou de 42°C (em vazio) para 71°C (em sobrecarga), o que ainda é considerável. No entanto, há que atender que a sobrecarga aplicada neste ensaio foi de $(Kₜ)%. 
"""

# ╔═╡ a73d0b8c-670b-4b0b-aa79-bca1f458d039


# ╔═╡ 4c2f7a61-ec74-48cd-9bb8-1d91e449c7ce
md"""
# Setup
"""

# ╔═╡ 5ee6970a-dbad-4e6f-8db1-0e982186a11a
TableOfContents(title="Índice", depth=4)

# ╔═╡ a790daa2-c97b-4bb3-9009-1a45026ddd5e
md"""
## Notação complexa
"""

# ╔═╡ 73cc066c-1576-426c-ab16-1b255b25929c
j = Base.im   # to use "j" as imaginary unit instead the Julia default "im"

# ╔═╡ 72dbc711-7c9b-4434-b672-ad42e402e23c
I⃗ₘ = Iₘ .* (cos.(-φ) + j*sin.(-φ))

# ╔═╡ ee81d813-b6dc-42a5-bc13-6dc9a57f510c
∠(θ) = cis(deg2rad(θ)) 		# to use phasor notation (with angle in degrees )

# ╔═╡ d975633d-7b1f-444f-90c4-89c1eec0cd93
I⃗₀ = (I₀)∠(-φ₀)

# ╔═╡ f642f5bf-d81c-46ed-a941-17c1ab104e8b
I⃗ᵣ = I⃗ₘ .- I⃗₀ 

# ╔═╡ 8edb6011-2655-496a-bb06-082e842b535c
Pd₁ = 3 * R₂ᵥ * ((1 .- s) ./ s) .* (abs.(I⃗ᵣ)).^2

# ╔═╡ ade3e711-547e-42a4-b5ba-f089025752b5
Td₁ = Pd₁ ./ (2π * nₘ / 60)

# ╔═╡ b6419e4b-652b-4134-8937-d76ff5d90795
begin
	scatter(nₘ, Tₘ, label= "lab (sensor de binário)", 
					title="Característica mecânica", 
					xlabel="velocidade (rpm)", ylabel="binário (Nm)")
	plot!(nₘ, Tₘ, lc=:blue, lw= 2, label=:none)
	plot!(nₘ, Tₘ₁, lc=:red, lw=2, label = "Binário (método indireto)")
	plot!(nₘ, Td₁, lc=:green, lw=2, line=:dash, label = "Binário desenvolvido")
	scatter!([1390], [Tₘₙ], label="nominal", mc=:purple)
end

# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
Plots = "91a5bcdd-55d7-5caf-9e0b-520d859cae80"
PlutoUI = "7f904dfe-b85e-4ff6-b463-dae2292396a8"
PrettyTables = "08abe8d2-0d0c-5749-adfa-8a2ac140af0d"

[compat]
Plots = "~1.40.3"
PlutoUI = "~0.7.58"
PrettyTables = "~2.3.1"
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.10.2"
manifest_format = "2.0"
project_hash = "1ccce30b39c160d45206df47c77e21890ab4e355"

[[deps.AbstractPlutoDingetjes]]
deps = ["Pkg"]
git-tree-sha1 = "0f748c81756f2e5e6854298f11ad8b2dfae6911a"
uuid = "6e696c72-6542-2067-7265-42206c756150"
version = "1.3.0"

[[deps.ArgTools]]
uuid = "0dad84c5-d112-42e6-8d28-ef12dabb789f"
version = "1.1.1"

[[deps.Artifacts]]
uuid = "56f22d72-fd6d-98f1-02f0-08ddc0907c33"

[[deps.Base64]]
uuid = "2a0f44e3-6c83-55bd-87e4-b1978d98bd5f"

[[deps.BitFlags]]
git-tree-sha1 = "2dc09997850d68179b69dafb58ae806167a32b1b"
uuid = "d1d4a3ce-64b1-5f1a-9ba4-7e7e69966f35"
version = "0.1.8"

[[deps.Bzip2_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg"]
git-tree-sha1 = "9e2a6b69137e6969bab0152632dcb3bc108c8bdd"
uuid = "6e34b625-4abd-537c-b88f-471c36dfa7a0"
version = "1.0.8+1"

[[deps.Cairo_jll]]
deps = ["Artifacts", "Bzip2_jll", "CompilerSupportLibraries_jll", "Fontconfig_jll", "FreeType2_jll", "Glib_jll", "JLLWrappers", "LZO_jll", "Libdl", "Pixman_jll", "Xorg_libXext_jll", "Xorg_libXrender_jll", "Zlib_jll", "libpng_jll"]
git-tree-sha1 = "a4c43f59baa34011e303e76f5c8c91bf58415aaf"
uuid = "83423d85-b0ee-5818-9007-b63ccbeb887a"
version = "1.18.0+1"

[[deps.CodecZlib]]
deps = ["TranscodingStreams", "Zlib_jll"]
git-tree-sha1 = "59939d8a997469ee05c4b4944560a820f9ba0d73"
uuid = "944b1d66-785c-5afd-91f1-9de20f533193"
version = "0.7.4"

[[deps.ColorSchemes]]
deps = ["ColorTypes", "ColorVectorSpace", "Colors", "FixedPointNumbers", "PrecompileTools", "Random"]
git-tree-sha1 = "67c1f244b991cad9b0aa4b7540fb758c2488b129"
uuid = "35d6a980-a343-548e-a6ea-1d62b119f2f4"
version = "3.24.0"

[[deps.ColorTypes]]
deps = ["FixedPointNumbers", "Random"]
git-tree-sha1 = "eb7f0f8307f71fac7c606984ea5fb2817275d6e4"
uuid = "3da002f7-5984-5a60-b8a6-cbb66c0b333f"
version = "0.11.4"

[[deps.ColorVectorSpace]]
deps = ["ColorTypes", "FixedPointNumbers", "LinearAlgebra", "Requires", "Statistics", "TensorCore"]
git-tree-sha1 = "a1f44953f2382ebb937d60dafbe2deea4bd23249"
uuid = "c3611d14-8923-5661-9e6a-0046d554d3a4"
version = "0.10.0"

    [deps.ColorVectorSpace.extensions]
    SpecialFunctionsExt = "SpecialFunctions"

    [deps.ColorVectorSpace.weakdeps]
    SpecialFunctions = "276daf66-3868-5448-9aa4-cd146d93841b"

[[deps.Colors]]
deps = ["ColorTypes", "FixedPointNumbers", "Reexport"]
git-tree-sha1 = "fc08e5930ee9a4e03f84bfb5211cb54e7769758a"
uuid = "5ae59095-9a9b-59fe-a467-6f913c188581"
version = "0.12.10"

[[deps.Compat]]
deps = ["TOML", "UUIDs"]
git-tree-sha1 = "c955881e3c981181362ae4088b35995446298b80"
uuid = "34da2185-b29b-5c13-b0c7-acf172513d20"
version = "4.14.0"
weakdeps = ["Dates", "LinearAlgebra"]

    [deps.Compat.extensions]
    CompatLinearAlgebraExt = "LinearAlgebra"

[[deps.CompilerSupportLibraries_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "e66e0078-7015-5450-92f7-15fbd957f2ae"
version = "1.1.0+0"

[[deps.ConcurrentUtilities]]
deps = ["Serialization", "Sockets"]
git-tree-sha1 = "6cbbd4d241d7e6579ab354737f4dd95ca43946e1"
uuid = "f0e56b4a-5159-44fe-b623-3e5288b988bb"
version = "2.4.1"

[[deps.Contour]]
git-tree-sha1 = "d05d9e7b7aedff4e5b51a029dced05cfb6125781"
uuid = "d38c429a-6771-53c6-b99e-75d170b6e991"
version = "0.6.2"

[[deps.Crayons]]
git-tree-sha1 = "249fe38abf76d48563e2f4556bebd215aa317e15"
uuid = "a8cc5b0e-0ffa-5ad4-8c14-923d3ee1735f"
version = "4.1.1"

[[deps.DataAPI]]
git-tree-sha1 = "abe83f3a2f1b857aac70ef8b269080af17764bbe"
uuid = "9a962f9c-6df0-11e9-0e5d-c546b8b5ee8a"
version = "1.16.0"

[[deps.DataStructures]]
deps = ["Compat", "InteractiveUtils", "OrderedCollections"]
git-tree-sha1 = "0f4b5d62a88d8f59003e43c25a8a90de9eb76317"
uuid = "864edb3b-99cc-5e75-8d2d-829cb0a9cfe8"
version = "0.18.18"

[[deps.DataValueInterfaces]]
git-tree-sha1 = "bfc1187b79289637fa0ef6d4436ebdfe6905cbd6"
uuid = "e2d170a0-9d28-54be-80f0-106bbe20a464"
version = "1.0.0"

[[deps.Dates]]
deps = ["Printf"]
uuid = "ade2ca70-3891-5945-98fb-dc099432e06a"

[[deps.DelimitedFiles]]
deps = ["Mmap"]
git-tree-sha1 = "9e2f36d3c96a820c678f2f1f1782582fcf685bae"
uuid = "8bb1440f-4735-579b-a4ab-409b98df4dab"
version = "1.9.1"

[[deps.DocStringExtensions]]
deps = ["LibGit2"]
git-tree-sha1 = "2fb1e02f2b635d0845df5d7c167fec4dd739b00d"
uuid = "ffbed154-4ef7-542d-bbb7-c09d3a79fcae"
version = "0.9.3"

[[deps.Downloads]]
deps = ["ArgTools", "FileWatching", "LibCURL", "NetworkOptions"]
uuid = "f43a241f-c20a-4ad4-852c-f6b1247861c6"
version = "1.6.0"

[[deps.EpollShim_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl"]
git-tree-sha1 = "8e9441ee83492030ace98f9789a654a6d0b1f643"
uuid = "2702e6a9-849d-5ed8-8c21-79e8b8f9ee43"
version = "0.0.20230411+0"

[[deps.ExceptionUnwrapping]]
deps = ["Test"]
git-tree-sha1 = "dcb08a0d93ec0b1cdc4af184b26b591e9695423a"
uuid = "460bff9d-24e4-43bc-9d9f-a8973cb893f4"
version = "0.1.10"

[[deps.Expat_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl"]
git-tree-sha1 = "4558ab818dcceaab612d1bb8c19cee87eda2b83c"
uuid = "2e619515-83b5-522b-bb60-26c02a35a201"
version = "2.5.0+0"

[[deps.FFMPEG]]
deps = ["FFMPEG_jll"]
git-tree-sha1 = "b57e3acbe22f8484b4b5ff66a7499717fe1a9cc8"
uuid = "c87230d0-a227-11e9-1b43-d7ebe4e7570a"
version = "0.4.1"

[[deps.FFMPEG_jll]]
deps = ["Artifacts", "Bzip2_jll", "FreeType2_jll", "FriBidi_jll", "JLLWrappers", "LAME_jll", "Libdl", "Ogg_jll", "OpenSSL_jll", "Opus_jll", "PCRE2_jll", "Zlib_jll", "libaom_jll", "libass_jll", "libfdk_aac_jll", "libvorbis_jll", "x264_jll", "x265_jll"]
git-tree-sha1 = "466d45dc38e15794ec7d5d63ec03d776a9aff36e"
uuid = "b22a6f82-2f65-5046-a5b2-351ab43fb4e5"
version = "4.4.4+1"

[[deps.FileWatching]]
uuid = "7b1f6079-737a-58dc-b8bc-7a2ca5c1b5ee"

[[deps.FixedPointNumbers]]
deps = ["Statistics"]
git-tree-sha1 = "335bfdceacc84c5cdf16aadc768aa5ddfc5383cc"
uuid = "53c48c17-4a7d-5ca2-90c5-79b7896eea93"
version = "0.8.4"

[[deps.Fontconfig_jll]]
deps = ["Artifacts", "Bzip2_jll", "Expat_jll", "FreeType2_jll", "JLLWrappers", "Libdl", "Libuuid_jll", "Pkg", "Zlib_jll"]
git-tree-sha1 = "21efd19106a55620a188615da6d3d06cd7f6ee03"
uuid = "a3f928ae-7b40-5064-980b-68af3947d34b"
version = "2.13.93+0"

[[deps.Format]]
git-tree-sha1 = "9c68794ef81b08086aeb32eeaf33531668d5f5fc"
uuid = "1fa38f19-a742-5d3f-a2b9-30dd87b9d5f8"
version = "1.3.7"

[[deps.FreeType2_jll]]
deps = ["Artifacts", "Bzip2_jll", "JLLWrappers", "Libdl", "Zlib_jll"]
git-tree-sha1 = "d8db6a5a2fe1381c1ea4ef2cab7c69c2de7f9ea0"
uuid = "d7e528f0-a631-5988-bf34-fe36492bcfd7"
version = "2.13.1+0"

[[deps.FriBidi_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg"]
git-tree-sha1 = "aa31987c2ba8704e23c6c8ba8a4f769d5d7e4f91"
uuid = "559328eb-81f9-559d-9380-de523a88c83c"
version = "1.0.10+0"

[[deps.GLFW_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Libglvnd_jll", "Xorg_libXcursor_jll", "Xorg_libXi_jll", "Xorg_libXinerama_jll", "Xorg_libXrandr_jll"]
git-tree-sha1 = "ff38ba61beff76b8f4acad8ab0c97ef73bb670cb"
uuid = "0656b61e-2033-5cc2-a64a-77c0f6c09b89"
version = "3.3.9+0"

[[deps.GR]]
deps = ["Artifacts", "Base64", "DelimitedFiles", "Downloads", "GR_jll", "HTTP", "JSON", "Libdl", "LinearAlgebra", "Pkg", "Preferences", "Printf", "Random", "Serialization", "Sockets", "TOML", "Tar", "Test", "UUIDs", "p7zip_jll"]
git-tree-sha1 = "3437ade7073682993e092ca570ad68a2aba26983"
uuid = "28b8d3ca-fb5f-59d9-8090-bfdbd6d07a71"
version = "0.73.3"

[[deps.GR_jll]]
deps = ["Artifacts", "Bzip2_jll", "Cairo_jll", "FFMPEG_jll", "Fontconfig_jll", "FreeType2_jll", "GLFW_jll", "JLLWrappers", "JpegTurbo_jll", "Libdl", "Libtiff_jll", "Pixman_jll", "Qt6Base_jll", "Zlib_jll", "libpng_jll"]
git-tree-sha1 = "a96d5c713e6aa28c242b0d25c1347e258d6541ab"
uuid = "d2c73de3-f751-5644-a686-071e5b155ba9"
version = "0.73.3+0"

[[deps.Gettext_jll]]
deps = ["Artifacts", "CompilerSupportLibraries_jll", "JLLWrappers", "Libdl", "Libiconv_jll", "Pkg", "XML2_jll"]
git-tree-sha1 = "9b02998aba7bf074d14de89f9d37ca24a1a0b046"
uuid = "78b55507-aeef-58d4-861c-77aaff3498b1"
version = "0.21.0+0"

[[deps.Glib_jll]]
deps = ["Artifacts", "Gettext_jll", "JLLWrappers", "Libdl", "Libffi_jll", "Libiconv_jll", "Libmount_jll", "PCRE2_jll", "Zlib_jll"]
git-tree-sha1 = "359a1ba2e320790ddbe4ee8b4d54a305c0ea2aff"
uuid = "7746bdde-850d-59dc-9ae8-88ece973131d"
version = "2.80.0+0"

[[deps.Graphite2_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg"]
git-tree-sha1 = "344bf40dcab1073aca04aa0df4fb092f920e4011"
uuid = "3b182d85-2403-5c21-9c21-1e1f0cc25472"
version = "1.3.14+0"

[[deps.Grisu]]
git-tree-sha1 = "53bb909d1151e57e2484c3d1b53e19552b887fb2"
uuid = "42e2da0e-8278-4e71-bc24-59509adca0fe"
version = "1.0.2"

[[deps.HTTP]]
deps = ["Base64", "CodecZlib", "ConcurrentUtilities", "Dates", "ExceptionUnwrapping", "Logging", "LoggingExtras", "MbedTLS", "NetworkOptions", "OpenSSL", "Random", "SimpleBufferStream", "Sockets", "URIs", "UUIDs"]
git-tree-sha1 = "8e59b47b9dc525b70550ca082ce85bcd7f5477cd"
uuid = "cd3eb016-35fb-5094-929b-558a96fad6f3"
version = "1.10.5"

[[deps.HarfBuzz_jll]]
deps = ["Artifacts", "Cairo_jll", "Fontconfig_jll", "FreeType2_jll", "Glib_jll", "Graphite2_jll", "JLLWrappers", "Libdl", "Libffi_jll", "Pkg"]
git-tree-sha1 = "129acf094d168394e80ee1dc4bc06ec835e510a3"
uuid = "2e76f6c2-a576-52d4-95c1-20adfe4de566"
version = "2.8.1+1"

[[deps.Hyperscript]]
deps = ["Test"]
git-tree-sha1 = "179267cfa5e712760cd43dcae385d7ea90cc25a4"
uuid = "47d2ed2b-36de-50cf-bf87-49c2cf4b8b91"
version = "0.0.5"

[[deps.HypertextLiteral]]
deps = ["Tricks"]
git-tree-sha1 = "7134810b1afce04bbc1045ca1985fbe81ce17653"
uuid = "ac1192a8-f4b3-4bfe-ba22-af5b92cd3ab2"
version = "0.9.5"

[[deps.IOCapture]]
deps = ["Logging", "Random"]
git-tree-sha1 = "8b72179abc660bfab5e28472e019392b97d0985c"
uuid = "b5f81e59-6552-4d32-b1f0-c071b021bf89"
version = "0.2.4"

[[deps.InteractiveUtils]]
deps = ["Markdown"]
uuid = "b77e0a4c-d291-57a0-90e8-8db25a27a240"

[[deps.IrrationalConstants]]
git-tree-sha1 = "630b497eafcc20001bba38a4651b327dcfc491d2"
uuid = "92d709cd-6900-40b7-9082-c6be49f344b6"
version = "0.2.2"

[[deps.IteratorInterfaceExtensions]]
git-tree-sha1 = "a3f24677c21f5bbe9d2a714f95dcd58337fb2856"
uuid = "82899510-4779-5014-852e-03e436cf321d"
version = "1.0.0"

[[deps.JLFzf]]
deps = ["Pipe", "REPL", "Random", "fzf_jll"]
git-tree-sha1 = "a53ebe394b71470c7f97c2e7e170d51df21b17af"
uuid = "1019f520-868f-41f5-a6de-eb00f4b6a39c"
version = "0.1.7"

[[deps.JLLWrappers]]
deps = ["Artifacts", "Preferences"]
git-tree-sha1 = "7e5d6779a1e09a36db2a7b6cff50942a0a7d0fca"
uuid = "692b3bcd-3c85-4b1f-b108-f13ce0eb3210"
version = "1.5.0"

[[deps.JSON]]
deps = ["Dates", "Mmap", "Parsers", "Unicode"]
git-tree-sha1 = "31e996f0a15c7b280ba9f76636b3ff9e2ae58c9a"
uuid = "682c06a0-de6a-54ab-a142-c8b1cf79cde6"
version = "0.21.4"

[[deps.JpegTurbo_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl"]
git-tree-sha1 = "3336abae9a713d2210bb57ab484b1e065edd7d23"
uuid = "aacddb02-875f-59d6-b918-886e6ef4fbf8"
version = "3.0.2+0"

[[deps.LAME_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg"]
git-tree-sha1 = "f6250b16881adf048549549fba48b1161acdac8c"
uuid = "c1c5ebd0-6772-5130-a774-d5fcae4a789d"
version = "3.100.1+0"

[[deps.LERC_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg"]
git-tree-sha1 = "bf36f528eec6634efc60d7ec062008f171071434"
uuid = "88015f11-f218-50d7-93a8-a6af411a945d"
version = "3.0.0+1"

[[deps.LLVMOpenMP_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl"]
git-tree-sha1 = "d986ce2d884d49126836ea94ed5bfb0f12679713"
uuid = "1d63c593-3942-5779-bab2-d838dc0a180e"
version = "15.0.7+0"

[[deps.LZO_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg"]
git-tree-sha1 = "e5b909bcf985c5e2605737d2ce278ed791b89be6"
uuid = "dd4b983a-f0e5-5f8d-a1b7-129d4a5fb1ac"
version = "2.10.1+0"

[[deps.LaTeXStrings]]
git-tree-sha1 = "50901ebc375ed41dbf8058da26f9de442febbbec"
uuid = "b964fa9f-0449-5b57-a5c2-d3ea65f4040f"
version = "1.3.1"

[[deps.Latexify]]
deps = ["Format", "InteractiveUtils", "LaTeXStrings", "MacroTools", "Markdown", "OrderedCollections", "Requires"]
git-tree-sha1 = "cad560042a7cc108f5a4c24ea1431a9221f22c1b"
uuid = "23fbe1c1-3f47-55db-b15f-69d7ec21a316"
version = "0.16.2"

    [deps.Latexify.extensions]
    DataFramesExt = "DataFrames"
    SymEngineExt = "SymEngine"

    [deps.Latexify.weakdeps]
    DataFrames = "a93c6f00-e57d-5684-b7b6-d8193f3e46c0"
    SymEngine = "123dc426-2d89-5057-bbad-38513e3affd8"

[[deps.LibCURL]]
deps = ["LibCURL_jll", "MozillaCACerts_jll"]
uuid = "b27032c2-a3e7-50c8-80cd-2d36dbcbfd21"
version = "0.6.4"

[[deps.LibCURL_jll]]
deps = ["Artifacts", "LibSSH2_jll", "Libdl", "MbedTLS_jll", "Zlib_jll", "nghttp2_jll"]
uuid = "deac9b47-8bc7-5906-a0fe-35ac56dc84c0"
version = "8.4.0+0"

[[deps.LibGit2]]
deps = ["Base64", "LibGit2_jll", "NetworkOptions", "Printf", "SHA"]
uuid = "76f85450-5226-5b5a-8eaa-529ad045b433"

[[deps.LibGit2_jll]]
deps = ["Artifacts", "LibSSH2_jll", "Libdl", "MbedTLS_jll"]
uuid = "e37daf67-58a4-590a-8e99-b0245dd2ffc5"
version = "1.6.4+0"

[[deps.LibSSH2_jll]]
deps = ["Artifacts", "Libdl", "MbedTLS_jll"]
uuid = "29816b5a-b9ab-546f-933c-edad1886dfa8"
version = "1.11.0+1"

[[deps.Libdl]]
uuid = "8f399da3-3557-5675-b5ff-fb832c97cbdb"

[[deps.Libffi_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg"]
git-tree-sha1 = "0b4a5d71f3e5200a7dff793393e09dfc2d874290"
uuid = "e9f186c6-92d2-5b65-8a66-fee21dc1b490"
version = "3.2.2+1"

[[deps.Libgcrypt_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Libgpg_error_jll", "Pkg"]
git-tree-sha1 = "64613c82a59c120435c067c2b809fc61cf5166ae"
uuid = "d4300ac3-e22c-5743-9152-c294e39db1e4"
version = "1.8.7+0"

[[deps.Libglvnd_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg", "Xorg_libX11_jll", "Xorg_libXext_jll"]
git-tree-sha1 = "6f73d1dd803986947b2c750138528a999a6c7733"
uuid = "7e76a0d4-f3c7-5321-8279-8d96eeed0f29"
version = "1.6.0+0"

[[deps.Libgpg_error_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg"]
git-tree-sha1 = "c333716e46366857753e273ce6a69ee0945a6db9"
uuid = "7add5ba3-2f88-524e-9cd5-f83b8a55f7b8"
version = "1.42.0+0"

[[deps.Libiconv_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl"]
git-tree-sha1 = "f9557a255370125b405568f9767d6d195822a175"
uuid = "94ce4f54-9a6c-5748-9c1c-f9c7231a4531"
version = "1.17.0+0"

[[deps.Libmount_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl"]
git-tree-sha1 = "dae976433497a2f841baadea93d27e68f1a12a97"
uuid = "4b2f31a3-9ecc-558c-b454-b3730dcb73e9"
version = "2.39.3+0"

[[deps.Libtiff_jll]]
deps = ["Artifacts", "JLLWrappers", "JpegTurbo_jll", "LERC_jll", "Libdl", "XZ_jll", "Zlib_jll", "Zstd_jll"]
git-tree-sha1 = "2da088d113af58221c52828a80378e16be7d037a"
uuid = "89763e89-9b03-5906-acba-b20f662cd828"
version = "4.5.1+1"

[[deps.Libuuid_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl"]
git-tree-sha1 = "0a04a1318df1bf510beb2562cf90fb0c386f58c4"
uuid = "38a345b3-de98-5d2b-a5d3-14cd9215e700"
version = "2.39.3+1"

[[deps.LinearAlgebra]]
deps = ["Libdl", "OpenBLAS_jll", "libblastrampoline_jll"]
uuid = "37e2e46d-f89d-539d-b4ee-838fcccc9c8e"

[[deps.LogExpFunctions]]
deps = ["DocStringExtensions", "IrrationalConstants", "LinearAlgebra"]
git-tree-sha1 = "18144f3e9cbe9b15b070288eef858f71b291ce37"
uuid = "2ab3a3ac-af41-5b50-aa03-7779005ae688"
version = "0.3.27"

    [deps.LogExpFunctions.extensions]
    LogExpFunctionsChainRulesCoreExt = "ChainRulesCore"
    LogExpFunctionsChangesOfVariablesExt = "ChangesOfVariables"
    LogExpFunctionsInverseFunctionsExt = "InverseFunctions"

    [deps.LogExpFunctions.weakdeps]
    ChainRulesCore = "d360d2e6-b24c-11e9-a2a3-2a2ae2dbcce4"
    ChangesOfVariables = "9e997f8a-9a97-42d5-a9f1-ce6bfc15e2c0"
    InverseFunctions = "3587e190-3f89-42d0-90ee-14403ec27112"

[[deps.Logging]]
uuid = "56ddb016-857b-54e1-b83d-db4d58db5568"

[[deps.LoggingExtras]]
deps = ["Dates", "Logging"]
git-tree-sha1 = "c1dd6d7978c12545b4179fb6153b9250c96b0075"
uuid = "e6f89c97-d47a-5376-807f-9c37f3926c36"
version = "1.0.3"

[[deps.MIMEs]]
git-tree-sha1 = "65f28ad4b594aebe22157d6fac869786a255b7eb"
uuid = "6c6e2e6c-3030-632d-7369-2d6c69616d65"
version = "0.1.4"

[[deps.MacroTools]]
deps = ["Markdown", "Random"]
git-tree-sha1 = "2fa9ee3e63fd3a4f7a9a4f4744a52f4856de82df"
uuid = "1914dd2f-81c6-5fcd-8719-6d5c9610ff09"
version = "0.5.13"

[[deps.Markdown]]
deps = ["Base64"]
uuid = "d6f4376e-aef5-505a-96c1-9c027394607a"

[[deps.MbedTLS]]
deps = ["Dates", "MbedTLS_jll", "MozillaCACerts_jll", "NetworkOptions", "Random", "Sockets"]
git-tree-sha1 = "c067a280ddc25f196b5e7df3877c6b226d390aaf"
uuid = "739be429-bea8-5141-9913-cc70e7f3736d"
version = "1.1.9"

[[deps.MbedTLS_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "c8ffd9c3-330d-5841-b78e-0817d7145fa1"
version = "2.28.2+1"

[[deps.Measures]]
git-tree-sha1 = "c13304c81eec1ed3af7fc20e75fb6b26092a1102"
uuid = "442fdcdd-2543-5da2-b0f3-8c86c306513e"
version = "0.3.2"

[[deps.Missings]]
deps = ["DataAPI"]
git-tree-sha1 = "f66bdc5de519e8f8ae43bdc598782d35a25b1272"
uuid = "e1d29d7a-bbdc-5cf2-9ac0-f12de2c33e28"
version = "1.1.0"

[[deps.Mmap]]
uuid = "a63ad114-7e13-5084-954f-fe012c677804"

[[deps.MozillaCACerts_jll]]
uuid = "14a3606d-f60d-562e-9121-12d972cd8159"
version = "2023.1.10"

[[deps.NaNMath]]
deps = ["OpenLibm_jll"]
git-tree-sha1 = "0877504529a3e5c3343c6f8b4c0381e57e4387e4"
uuid = "77ba4419-2d1f-58cd-9bb1-8ffee604a2e3"
version = "1.0.2"

[[deps.NetworkOptions]]
uuid = "ca575930-c2e3-43a9-ace4-1e988b2c1908"
version = "1.2.0"

[[deps.Ogg_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg"]
git-tree-sha1 = "887579a3eb005446d514ab7aeac5d1d027658b8f"
uuid = "e7412a2a-1a6e-54c0-be00-318e2571c051"
version = "1.3.5+1"

[[deps.OpenBLAS_jll]]
deps = ["Artifacts", "CompilerSupportLibraries_jll", "Libdl"]
uuid = "4536629a-c528-5b80-bd46-f80d51c5b363"
version = "0.3.23+4"

[[deps.OpenLibm_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "05823500-19ac-5b8b-9628-191a04bc5112"
version = "0.8.1+2"

[[deps.OpenSSL]]
deps = ["BitFlags", "Dates", "MozillaCACerts_jll", "OpenSSL_jll", "Sockets"]
git-tree-sha1 = "af81a32750ebc831ee28bdaaba6e1067decef51e"
uuid = "4d8831e6-92b7-49fb-bdf8-b643e874388c"
version = "1.4.2"

[[deps.OpenSSL_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl"]
git-tree-sha1 = "3da7367955dcc5c54c1ba4d402ccdc09a1a3e046"
uuid = "458c3c95-2e84-50aa-8efc-19380b2a3a95"
version = "3.0.13+1"

[[deps.Opus_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg"]
git-tree-sha1 = "51a08fb14ec28da2ec7a927c4337e4332c2a4720"
uuid = "91d4177d-7536-5919-b921-800302f37372"
version = "1.3.2+0"

[[deps.OrderedCollections]]
git-tree-sha1 = "dfdf5519f235516220579f949664f1bf44e741c5"
uuid = "bac558e1-5e72-5ebc-8fee-abe8a469f55d"
version = "1.6.3"

[[deps.PCRE2_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "efcefdf7-47ab-520b-bdef-62a2eaa19f15"
version = "10.42.0+1"

[[deps.Parsers]]
deps = ["Dates", "PrecompileTools", "UUIDs"]
git-tree-sha1 = "8489905bcdbcfac64d1daa51ca07c0d8f0283821"
uuid = "69de0a69-1ddd-5017-9359-2bf0b02dc9f0"
version = "2.8.1"

[[deps.Pipe]]
git-tree-sha1 = "6842804e7867b115ca9de748a0cf6b364523c16d"
uuid = "b98c9c47-44ae-5843-9183-064241ee97a0"
version = "1.3.0"

[[deps.Pixman_jll]]
deps = ["Artifacts", "CompilerSupportLibraries_jll", "JLLWrappers", "LLVMOpenMP_jll", "Libdl"]
git-tree-sha1 = "64779bc4c9784fee475689a1752ef4d5747c5e87"
uuid = "30392449-352a-5448-841d-b1acce4e97dc"
version = "0.42.2+0"

[[deps.Pkg]]
deps = ["Artifacts", "Dates", "Downloads", "FileWatching", "LibGit2", "Libdl", "Logging", "Markdown", "Printf", "REPL", "Random", "SHA", "Serialization", "TOML", "Tar", "UUIDs", "p7zip_jll"]
uuid = "44cfe95a-1eb2-52ea-b672-e2afdf69b78f"
version = "1.10.0"

[[deps.PlotThemes]]
deps = ["PlotUtils", "Statistics"]
git-tree-sha1 = "1f03a2d339f42dca4a4da149c7e15e9b896ad899"
uuid = "ccf2f8ad-2431-5c83-bf29-c5338b663b6a"
version = "3.1.0"

[[deps.PlotUtils]]
deps = ["ColorSchemes", "Colors", "Dates", "PrecompileTools", "Printf", "Random", "Reexport", "Statistics"]
git-tree-sha1 = "7b1a9df27f072ac4c9c7cbe5efb198489258d1f5"
uuid = "995b91a9-d308-5afd-9ec6-746e21dbc043"
version = "1.4.1"

[[deps.Plots]]
deps = ["Base64", "Contour", "Dates", "Downloads", "FFMPEG", "FixedPointNumbers", "GR", "JLFzf", "JSON", "LaTeXStrings", "Latexify", "LinearAlgebra", "Measures", "NaNMath", "Pkg", "PlotThemes", "PlotUtils", "PrecompileTools", "Printf", "REPL", "Random", "RecipesBase", "RecipesPipeline", "Reexport", "RelocatableFolders", "Requires", "Scratch", "Showoff", "SparseArrays", "Statistics", "StatsBase", "UUIDs", "UnicodeFun", "UnitfulLatexify", "Unzip"]
git-tree-sha1 = "3bdfa4fa528ef21287ef659a89d686e8a1bcb1a9"
uuid = "91a5bcdd-55d7-5caf-9e0b-520d859cae80"
version = "1.40.3"

    [deps.Plots.extensions]
    FileIOExt = "FileIO"
    GeometryBasicsExt = "GeometryBasics"
    IJuliaExt = "IJulia"
    ImageInTerminalExt = "ImageInTerminal"
    UnitfulExt = "Unitful"

    [deps.Plots.weakdeps]
    FileIO = "5789e2e9-d7fb-5bc7-8068-2c6fae9b9549"
    GeometryBasics = "5c1252a2-5f33-56bf-86c9-59e7332b4326"
    IJulia = "7073ff75-c697-5162-941a-fcdaad2a7d2a"
    ImageInTerminal = "d8c32880-2388-543b-8c61-d9f865259254"
    Unitful = "1986cc42-f94f-5a68-af5c-568840ba703d"

[[deps.PlutoUI]]
deps = ["AbstractPlutoDingetjes", "Base64", "ColorTypes", "Dates", "FixedPointNumbers", "Hyperscript", "HypertextLiteral", "IOCapture", "InteractiveUtils", "JSON", "Logging", "MIMEs", "Markdown", "Random", "Reexport", "URIs", "UUIDs"]
git-tree-sha1 = "71a22244e352aa8c5f0f2adde4150f62368a3f2e"
uuid = "7f904dfe-b85e-4ff6-b463-dae2292396a8"
version = "0.7.58"

[[deps.PrecompileTools]]
deps = ["Preferences"]
git-tree-sha1 = "5aa36f7049a63a1528fe8f7c3f2113413ffd4e1f"
uuid = "aea7be01-6a6a-4083-8856-8a6e6704d82a"
version = "1.2.1"

[[deps.Preferences]]
deps = ["TOML"]
git-tree-sha1 = "9306f6085165d270f7e3db02af26a400d580f5c6"
uuid = "21216c6a-2e73-6563-6e65-726566657250"
version = "1.4.3"

[[deps.PrettyTables]]
deps = ["Crayons", "LaTeXStrings", "Markdown", "PrecompileTools", "Printf", "Reexport", "StringManipulation", "Tables"]
git-tree-sha1 = "88b895d13d53b5577fd53379d913b9ab9ac82660"
uuid = "08abe8d2-0d0c-5749-adfa-8a2ac140af0d"
version = "2.3.1"

[[deps.Printf]]
deps = ["Unicode"]
uuid = "de0858da-6303-5e67-8744-51eddeeeb8d7"

[[deps.Qt6Base_jll]]
deps = ["Artifacts", "CompilerSupportLibraries_jll", "Fontconfig_jll", "Glib_jll", "JLLWrappers", "Libdl", "Libglvnd_jll", "OpenSSL_jll", "Vulkan_Loader_jll", "Xorg_libSM_jll", "Xorg_libXext_jll", "Xorg_libXrender_jll", "Xorg_libxcb_jll", "Xorg_xcb_util_cursor_jll", "Xorg_xcb_util_image_jll", "Xorg_xcb_util_keysyms_jll", "Xorg_xcb_util_renderutil_jll", "Xorg_xcb_util_wm_jll", "Zlib_jll", "libinput_jll", "xkbcommon_jll"]
git-tree-sha1 = "37b7bb7aabf9a085e0044307e1717436117f2b3b"
uuid = "c0090381-4147-56d7-9ebc-da0b1113ec56"
version = "6.5.3+1"

[[deps.REPL]]
deps = ["InteractiveUtils", "Markdown", "Sockets", "Unicode"]
uuid = "3fa0cd96-eef1-5676-8a61-b3b8758bbffb"

[[deps.Random]]
deps = ["SHA"]
uuid = "9a3f8284-a2c9-5f02-9a11-845980a1fd5c"

[[deps.RecipesBase]]
deps = ["PrecompileTools"]
git-tree-sha1 = "5c3d09cc4f31f5fc6af001c250bf1278733100ff"
uuid = "3cdcf5f2-1ef4-517c-9805-6587b60abb01"
version = "1.3.4"

[[deps.RecipesPipeline]]
deps = ["Dates", "NaNMath", "PlotUtils", "PrecompileTools", "RecipesBase"]
git-tree-sha1 = "45cf9fd0ca5839d06ef333c8201714e888486342"
uuid = "01d81517-befc-4cb6-b9ec-a95719d0359c"
version = "0.6.12"

[[deps.Reexport]]
git-tree-sha1 = "45e428421666073eab6f2da5c9d310d99bb12f9b"
uuid = "189a3867-3050-52da-a836-e630ba90ab69"
version = "1.2.2"

[[deps.RelocatableFolders]]
deps = ["SHA", "Scratch"]
git-tree-sha1 = "ffdaf70d81cf6ff22c2b6e733c900c3321cab864"
uuid = "05181044-ff0b-4ac5-8273-598c1e38db00"
version = "1.0.1"

[[deps.Requires]]
deps = ["UUIDs"]
git-tree-sha1 = "838a3a4188e2ded87a4f9f184b4b0d78a1e91cb7"
uuid = "ae029012-a4dd-5104-9daa-d747884805df"
version = "1.3.0"

[[deps.SHA]]
uuid = "ea8e919c-243c-51af-8825-aaa63cd721ce"
version = "0.7.0"

[[deps.Scratch]]
deps = ["Dates"]
git-tree-sha1 = "3bac05bc7e74a75fd9cba4295cde4045d9fe2386"
uuid = "6c6a2e73-6563-6170-7368-637461726353"
version = "1.2.1"

[[deps.Serialization]]
uuid = "9e88b42a-f829-5b0c-bbe9-9e923198166b"

[[deps.Showoff]]
deps = ["Dates", "Grisu"]
git-tree-sha1 = "91eddf657aca81df9ae6ceb20b959ae5653ad1de"
uuid = "992d4aef-0814-514b-bc4d-f2e9a6c4116f"
version = "1.0.3"

[[deps.SimpleBufferStream]]
git-tree-sha1 = "874e8867b33a00e784c8a7e4b60afe9e037b74e1"
uuid = "777ac1f9-54b0-4bf8-805c-2214025038e7"
version = "1.1.0"

[[deps.Sockets]]
uuid = "6462fe0b-24de-5631-8697-dd941f90decc"

[[deps.SortingAlgorithms]]
deps = ["DataStructures"]
git-tree-sha1 = "66e0a8e672a0bdfca2c3f5937efb8538b9ddc085"
uuid = "a2af1166-a08f-5f64-846c-94a0d3cef48c"
version = "1.2.1"

[[deps.SparseArrays]]
deps = ["Libdl", "LinearAlgebra", "Random", "Serialization", "SuiteSparse_jll"]
uuid = "2f01184e-e22b-5df5-ae63-d93ebab69eaf"
version = "1.10.0"

[[deps.Statistics]]
deps = ["LinearAlgebra", "SparseArrays"]
uuid = "10745b16-79ce-11e8-11f9-7d13ad32a3b2"
version = "1.10.0"

[[deps.StatsAPI]]
deps = ["LinearAlgebra"]
git-tree-sha1 = "1ff449ad350c9c4cbc756624d6f8a8c3ef56d3ed"
uuid = "82ae8749-77ed-4fe6-ae5f-f523153014b0"
version = "1.7.0"

[[deps.StatsBase]]
deps = ["DataAPI", "DataStructures", "LinearAlgebra", "LogExpFunctions", "Missings", "Printf", "Random", "SortingAlgorithms", "SparseArrays", "Statistics", "StatsAPI"]
git-tree-sha1 = "1d77abd07f617c4868c33d4f5b9e1dbb2643c9cf"
uuid = "2913bbd2-ae8a-5f71-8c99-4fb6c76f3a91"
version = "0.34.2"

[[deps.StringManipulation]]
deps = ["PrecompileTools"]
git-tree-sha1 = "a04cabe79c5f01f4d723cc6704070ada0b9d46d5"
uuid = "892a3eda-7b42-436c-8928-eab12a02cf0e"
version = "0.3.4"

[[deps.SuiteSparse_jll]]
deps = ["Artifacts", "Libdl", "libblastrampoline_jll"]
uuid = "bea87d4a-7f5b-5778-9afe-8cc45184846c"
version = "7.2.1+1"

[[deps.TOML]]
deps = ["Dates"]
uuid = "fa267f1f-6049-4f14-aa54-33bafae1ed76"
version = "1.0.3"

[[deps.TableTraits]]
deps = ["IteratorInterfaceExtensions"]
git-tree-sha1 = "c06b2f539df1c6efa794486abfb6ed2022561a39"
uuid = "3783bdb8-4a98-5b6b-af9a-565f29a5fe9c"
version = "1.0.1"

[[deps.Tables]]
deps = ["DataAPI", "DataValueInterfaces", "IteratorInterfaceExtensions", "LinearAlgebra", "OrderedCollections", "TableTraits"]
git-tree-sha1 = "cb76cf677714c095e535e3501ac7954732aeea2d"
uuid = "bd369af6-aec1-5ad0-b16a-f7cc5008161c"
version = "1.11.1"

[[deps.Tar]]
deps = ["ArgTools", "SHA"]
uuid = "a4e569a6-e804-4fa4-b0f3-eef7a1d5b13e"
version = "1.10.0"

[[deps.TensorCore]]
deps = ["LinearAlgebra"]
git-tree-sha1 = "1feb45f88d133a655e001435632f019a9a1bcdb6"
uuid = "62fd8b95-f654-4bbd-a8a5-9c27f68ccd50"
version = "0.1.1"

[[deps.Test]]
deps = ["InteractiveUtils", "Logging", "Random", "Serialization"]
uuid = "8dfed614-e22c-5e08-85e1-65c5234f0b40"

[[deps.TranscodingStreams]]
git-tree-sha1 = "71509f04d045ec714c4748c785a59045c3736349"
uuid = "3bb67fe8-82b1-5028-8e26-92a6c54297fa"
version = "0.10.7"
weakdeps = ["Random", "Test"]

    [deps.TranscodingStreams.extensions]
    TestExt = ["Test", "Random"]

[[deps.Tricks]]
git-tree-sha1 = "eae1bb484cd63b36999ee58be2de6c178105112f"
uuid = "410a4b4d-49e4-4fbc-ab6d-cb71b17b3775"
version = "0.1.8"

[[deps.URIs]]
git-tree-sha1 = "67db6cc7b3821e19ebe75791a9dd19c9b1188f2b"
uuid = "5c2747f8-b7ea-4ff2-ba2e-563bfd36b1d4"
version = "1.5.1"

[[deps.UUIDs]]
deps = ["Random", "SHA"]
uuid = "cf7118a7-6976-5b1a-9a39-7adc72f591a4"

[[deps.Unicode]]
uuid = "4ec0a83e-493e-50e2-b9ac-8f72acf5a8f5"

[[deps.UnicodeFun]]
deps = ["REPL"]
git-tree-sha1 = "53915e50200959667e78a92a418594b428dffddf"
uuid = "1cfade01-22cf-5700-b092-accc4b62d6e1"
version = "0.4.1"

[[deps.Unitful]]
deps = ["Dates", "LinearAlgebra", "Random"]
git-tree-sha1 = "3c793be6df9dd77a0cf49d80984ef9ff996948fa"
uuid = "1986cc42-f94f-5a68-af5c-568840ba703d"
version = "1.19.0"

    [deps.Unitful.extensions]
    ConstructionBaseUnitfulExt = "ConstructionBase"
    InverseFunctionsUnitfulExt = "InverseFunctions"

    [deps.Unitful.weakdeps]
    ConstructionBase = "187b0558-2788-49d3-abe0-74a17ed4e7c9"
    InverseFunctions = "3587e190-3f89-42d0-90ee-14403ec27112"

[[deps.UnitfulLatexify]]
deps = ["LaTeXStrings", "Latexify", "Unitful"]
git-tree-sha1 = "e2d817cc500e960fdbafcf988ac8436ba3208bfd"
uuid = "45397f5d-5981-4c77-b2b3-fc36d6e9b728"
version = "1.6.3"

[[deps.Unzip]]
git-tree-sha1 = "ca0969166a028236229f63514992fc073799bb78"
uuid = "41fe7b60-77ed-43a1-b4f0-825fd5a5650d"
version = "0.2.0"

[[deps.Vulkan_Loader_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Wayland_jll", "Xorg_libX11_jll", "Xorg_libXrandr_jll", "xkbcommon_jll"]
git-tree-sha1 = "2f0486047a07670caad3a81a075d2e518acc5c59"
uuid = "a44049a8-05dd-5a78-86c9-5fde0876e88c"
version = "1.3.243+0"

[[deps.Wayland_jll]]
deps = ["Artifacts", "EpollShim_jll", "Expat_jll", "JLLWrappers", "Libdl", "Libffi_jll", "Pkg", "XML2_jll"]
git-tree-sha1 = "7558e29847e99bc3f04d6569e82d0f5c54460703"
uuid = "a2964d1f-97da-50d4-b82a-358c7fce9d89"
version = "1.21.0+1"

[[deps.Wayland_protocols_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg"]
git-tree-sha1 = "93f43ab61b16ddfb2fd3bb13b3ce241cafb0e6c9"
uuid = "2381bf8a-dfd0-557d-9999-79630e7b1b91"
version = "1.31.0+0"

[[deps.XML2_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Libiconv_jll", "Zlib_jll"]
git-tree-sha1 = "532e22cf7be8462035d092ff21fada7527e2c488"
uuid = "02c8fc9c-b97f-50b9-bbe4-9be30ff0a78a"
version = "2.12.6+0"

[[deps.XSLT_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Libgcrypt_jll", "Libgpg_error_jll", "Libiconv_jll", "Pkg", "XML2_jll", "Zlib_jll"]
git-tree-sha1 = "91844873c4085240b95e795f692c4cec4d805f8a"
uuid = "aed1982a-8fda-507f-9586-7b0439959a61"
version = "1.1.34+0"

[[deps.XZ_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl"]
git-tree-sha1 = "ac88fb95ae6447c8dda6a5503f3bafd496ae8632"
uuid = "ffd25f8a-64ca-5728-b0f7-c24cf3aae800"
version = "5.4.6+0"

[[deps.Xorg_libICE_jll]]
deps = ["Libdl", "Pkg"]
git-tree-sha1 = "e5becd4411063bdcac16be8b66fc2f9f6f1e8fe5"
uuid = "f67eecfb-183a-506d-b269-f58e52b52d7c"
version = "1.0.10+1"

[[deps.Xorg_libSM_jll]]
deps = ["Libdl", "Pkg", "Xorg_libICE_jll"]
git-tree-sha1 = "4a9d9e4c180e1e8119b5ffc224a7b59d3a7f7e18"
uuid = "c834827a-8449-5923-a945-d239c165b7dd"
version = "1.2.3+0"

[[deps.Xorg_libX11_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Xorg_libxcb_jll", "Xorg_xtrans_jll"]
git-tree-sha1 = "afead5aba5aa507ad5a3bf01f58f82c8d1403495"
uuid = "4f6342f7-b3d2-589e-9d20-edeb45f2b2bc"
version = "1.8.6+0"

[[deps.Xorg_libXau_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl"]
git-tree-sha1 = "6035850dcc70518ca32f012e46015b9beeda49d8"
uuid = "0c0b7dd1-d40b-584c-a123-a41640f87eec"
version = "1.0.11+0"

[[deps.Xorg_libXcursor_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg", "Xorg_libXfixes_jll", "Xorg_libXrender_jll"]
git-tree-sha1 = "12e0eb3bc634fa2080c1c37fccf56f7c22989afd"
uuid = "935fb764-8cf2-53bf-bb30-45bb1f8bf724"
version = "1.2.0+4"

[[deps.Xorg_libXdmcp_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl"]
git-tree-sha1 = "34d526d318358a859d7de23da945578e8e8727b7"
uuid = "a3789734-cfe1-5b06-b2d0-1dd0d9d62d05"
version = "1.1.4+0"

[[deps.Xorg_libXext_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg", "Xorg_libX11_jll"]
git-tree-sha1 = "b7c0aa8c376b31e4852b360222848637f481f8c3"
uuid = "1082639a-0dae-5f34-9b06-72781eeb8cb3"
version = "1.3.4+4"

[[deps.Xorg_libXfixes_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg", "Xorg_libX11_jll"]
git-tree-sha1 = "0e0dc7431e7a0587559f9294aeec269471c991a4"
uuid = "d091e8ba-531a-589c-9de9-94069b037ed8"
version = "5.0.3+4"

[[deps.Xorg_libXi_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg", "Xorg_libXext_jll", "Xorg_libXfixes_jll"]
git-tree-sha1 = "89b52bc2160aadc84d707093930ef0bffa641246"
uuid = "a51aa0fd-4e3c-5386-b890-e753decda492"
version = "1.7.10+4"

[[deps.Xorg_libXinerama_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg", "Xorg_libXext_jll"]
git-tree-sha1 = "26be8b1c342929259317d8b9f7b53bf2bb73b123"
uuid = "d1454406-59df-5ea1-beac-c340f2130bc3"
version = "1.1.4+4"

[[deps.Xorg_libXrandr_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg", "Xorg_libXext_jll", "Xorg_libXrender_jll"]
git-tree-sha1 = "34cea83cb726fb58f325887bf0612c6b3fb17631"
uuid = "ec84b674-ba8e-5d96-8ba1-2a689ba10484"
version = "1.5.2+4"

[[deps.Xorg_libXrender_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg", "Xorg_libX11_jll"]
git-tree-sha1 = "19560f30fd49f4d4efbe7002a1037f8c43d43b96"
uuid = "ea2f1a96-1ddc-540d-b46f-429655e07cfa"
version = "0.9.10+4"

[[deps.Xorg_libpthread_stubs_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl"]
git-tree-sha1 = "8fdda4c692503d44d04a0603d9ac0982054635f9"
uuid = "14d82f49-176c-5ed1-bb49-ad3f5cbd8c74"
version = "0.1.1+0"

[[deps.Xorg_libxcb_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "XSLT_jll", "Xorg_libXau_jll", "Xorg_libXdmcp_jll", "Xorg_libpthread_stubs_jll"]
git-tree-sha1 = "b4bfde5d5b652e22b9c790ad00af08b6d042b97d"
uuid = "c7cfdc94-dc32-55de-ac96-5a1b8d977c5b"
version = "1.15.0+0"

[[deps.Xorg_libxkbfile_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Xorg_libX11_jll"]
git-tree-sha1 = "730eeca102434283c50ccf7d1ecdadf521a765a4"
uuid = "cc61e674-0454-545c-8b26-ed2c68acab7a"
version = "1.1.2+0"

[[deps.Xorg_xcb_util_cursor_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Xorg_xcb_util_image_jll", "Xorg_xcb_util_jll", "Xorg_xcb_util_renderutil_jll"]
git-tree-sha1 = "04341cb870f29dcd5e39055f895c39d016e18ccd"
uuid = "e920d4aa-a673-5f3a-b3d7-f755a4d47c43"
version = "0.1.4+0"

[[deps.Xorg_xcb_util_image_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg", "Xorg_xcb_util_jll"]
git-tree-sha1 = "0fab0a40349ba1cba2c1da699243396ff8e94b97"
uuid = "12413925-8142-5f55-bb0e-6d7ca50bb09b"
version = "0.4.0+1"

[[deps.Xorg_xcb_util_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg", "Xorg_libxcb_jll"]
git-tree-sha1 = "e7fd7b2881fa2eaa72717420894d3938177862d1"
uuid = "2def613f-5ad1-5310-b15b-b15d46f528f5"
version = "0.4.0+1"

[[deps.Xorg_xcb_util_keysyms_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg", "Xorg_xcb_util_jll"]
git-tree-sha1 = "d1151e2c45a544f32441a567d1690e701ec89b00"
uuid = "975044d2-76e6-5fbe-bf08-97ce7c6574c7"
version = "0.4.0+1"

[[deps.Xorg_xcb_util_renderutil_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg", "Xorg_xcb_util_jll"]
git-tree-sha1 = "dfd7a8f38d4613b6a575253b3174dd991ca6183e"
uuid = "0d47668e-0667-5a69-a72c-f761630bfb7e"
version = "0.3.9+1"

[[deps.Xorg_xcb_util_wm_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg", "Xorg_xcb_util_jll"]
git-tree-sha1 = "e78d10aab01a4a154142c5006ed44fd9e8e31b67"
uuid = "c22f9ab0-d5fe-5066-847c-f4bb1cd4e361"
version = "0.4.1+1"

[[deps.Xorg_xkbcomp_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Xorg_libxkbfile_jll"]
git-tree-sha1 = "330f955bc41bb8f5270a369c473fc4a5a4e4d3cb"
uuid = "35661453-b289-5fab-8a00-3d9160c6a3a4"
version = "1.4.6+0"

[[deps.Xorg_xkeyboard_config_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Xorg_xkbcomp_jll"]
git-tree-sha1 = "691634e5453ad362044e2ad653e79f3ee3bb98c3"
uuid = "33bec58e-1273-512f-9401-5d533626f822"
version = "2.39.0+0"

[[deps.Xorg_xtrans_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl"]
git-tree-sha1 = "e92a1a012a10506618f10b7047e478403a046c77"
uuid = "c5fb5394-a638-5e4d-96e5-b29de1b5cf10"
version = "1.5.0+0"

[[deps.Zlib_jll]]
deps = ["Libdl"]
uuid = "83775a58-1f1d-513f-b197-d71354ab007a"
version = "1.2.13+1"

[[deps.Zstd_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl"]
git-tree-sha1 = "e678132f07ddb5bfa46857f0d7620fb9be675d3b"
uuid = "3161d3a3-bdf6-5164-811a-617609db77b4"
version = "1.5.6+0"

[[deps.eudev_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg", "gperf_jll"]
git-tree-sha1 = "431b678a28ebb559d224c0b6b6d01afce87c51ba"
uuid = "35ca27e7-8b34-5b7f-bca9-bdc33f59eb06"
version = "3.2.9+0"

[[deps.fzf_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl"]
git-tree-sha1 = "a68c9655fbe6dfcab3d972808f1aafec151ce3f8"
uuid = "214eeab7-80f7-51ab-84ad-2988db7cef09"
version = "0.43.0+0"

[[deps.gperf_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg"]
git-tree-sha1 = "3516a5630f741c9eecb3720b1ec9d8edc3ecc033"
uuid = "1a1c6b14-54f6-533d-8383-74cd7377aa70"
version = "3.1.1+0"

[[deps.libaom_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg"]
git-tree-sha1 = "3a2ea60308f0996d26f1e5354e10c24e9ef905d4"
uuid = "a4ae2306-e953-59d6-aa16-d00cac43593b"
version = "3.4.0+0"

[[deps.libass_jll]]
deps = ["Artifacts", "Bzip2_jll", "FreeType2_jll", "FriBidi_jll", "HarfBuzz_jll", "JLLWrappers", "Libdl", "Pkg", "Zlib_jll"]
git-tree-sha1 = "5982a94fcba20f02f42ace44b9894ee2b140fe47"
uuid = "0ac62f75-1d6f-5e53-bd7c-93b484bb37c0"
version = "0.15.1+0"

[[deps.libblastrampoline_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "8e850b90-86db-534c-a0d3-1478176c7d93"
version = "5.8.0+1"

[[deps.libevdev_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg"]
git-tree-sha1 = "141fe65dc3efabb0b1d5ba74e91f6ad26f84cc22"
uuid = "2db6ffa8-e38f-5e21-84af-90c45d0032cc"
version = "1.11.0+0"

[[deps.libfdk_aac_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg"]
git-tree-sha1 = "daacc84a041563f965be61859a36e17c4e4fcd55"
uuid = "f638f0a6-7fb0-5443-88ba-1cc74229b280"
version = "2.0.2+0"

[[deps.libinput_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg", "eudev_jll", "libevdev_jll", "mtdev_jll"]
git-tree-sha1 = "ad50e5b90f222cfe78aa3d5183a20a12de1322ce"
uuid = "36db933b-70db-51c0-b978-0f229ee0e533"
version = "1.18.0+0"

[[deps.libpng_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Zlib_jll"]
git-tree-sha1 = "d7015d2e18a5fd9a4f47de711837e980519781a4"
uuid = "b53b4c65-9356-5827-b1ea-8c7a1a84506f"
version = "1.6.43+1"

[[deps.libvorbis_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Ogg_jll", "Pkg"]
git-tree-sha1 = "b910cb81ef3fe6e78bf6acee440bda86fd6ae00c"
uuid = "f27f6e37-5d2b-51aa-960f-b287f2bc3b7a"
version = "1.3.7+1"

[[deps.mtdev_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg"]
git-tree-sha1 = "814e154bdb7be91d78b6802843f76b6ece642f11"
uuid = "009596ad-96f7-51b1-9f1b-5ce2d5e8a71e"
version = "1.1.6+0"

[[deps.nghttp2_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "8e850ede-7688-5339-a07c-302acd2aaf8d"
version = "1.52.0+1"

[[deps.p7zip_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "3f19e933-33d8-53b3-aaab-bd5110c3b7a0"
version = "17.4.0+2"

[[deps.x264_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg"]
git-tree-sha1 = "4fea590b89e6ec504593146bf8b988b2c00922b2"
uuid = "1270edf5-f2f9-52d2-97e9-ab00b5d0237a"
version = "2021.5.5+0"

[[deps.x265_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg"]
git-tree-sha1 = "ee567a171cce03570d77ad3a43e90218e38937a9"
uuid = "dfaa095f-4041-5dcd-9319-2fabd8486b76"
version = "3.5.0+0"

[[deps.xkbcommon_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg", "Wayland_jll", "Wayland_protocols_jll", "Xorg_libxcb_jll", "Xorg_xkeyboard_config_jll"]
git-tree-sha1 = "9c304562909ab2bab0262639bd4f444d7bc2be37"
uuid = "d8fb68d0-12a3-5cfd-a85a-d49703b185fd"
version = "1.4.1+1"
"""

# ╔═╡ Cell order:
# ╟─3dcbcdb3-0971-4bd3-8a9b-251ccef4dcc6
# ╟─5672df31-2195-45e8-be61-f84469e758c5
# ╟─9abde5bc-f8ac-4d0d-ba9a-82e09eb8d6b0
# ╟─ece18487-c3b1-426e-8765-9999a8f1fac0
# ╟─15f9a593-452e-4c37-9eb0-166a6727a875
# ╟─829eb1fb-25e4-4508-b674-5fd842b0a7db
# ╟─401f5b75-c2b8-44c7-a89b-e8dc8610a915
# ╟─503555a3-c445-4047-a065-dac940755acb
# ╟─70966e56-3cf9-4785-98f8-98ed052c964d
# ╠═69fe9f61-6b63-4386-9f22-5d07485b897d
# ╠═69fd01ff-848d-497e-a9c3-4ee8257b59f3
# ╠═e0bc73f5-847c-4987-99cc-f1094cfc86ba
# ╟─2c88e77d-430f-4a91-828d-2087c3657842
# ╟─54d36fb9-f5fd-4b0e-91ed-e3468bf503e4
# ╟─1ddb9524-1ffa-45e8-9e13-5e4c858de917
# ╠═e5eb112c-4b35-49a1-84e0-be90201728de
# ╠═552314d1-08da-4374-9281-cbab23505917
# ╟─d7146b57-78f8-4abd-9df3-bc73e4fc5ce0
# ╟─24c470e9-5b20-4d1e-906c-d6e6eaab867d
# ╟─81f9941a-9011-48b0-9237-7d8e6c6dfae5
# ╠═2e89f897-bf0b-4756-8c9b-a37c37d68943
# ╟─9a4b4736-038c-4167-8b8d-41beb2cfbd50
# ╟─bd3f5136-789a-4ffe-bfe1-473846e2accb
# ╟─4ce12df2-4b03-4e3a-9802-6decb834a648
# ╟─23bcdd74-0ed8-4cf2-94b7-26f262069de4
# ╟─6dea539c-3a5c-4b35-a38d-842b4d945172
# ╠═c08064f0-c2a1-492d-8baf-70337f494cf2
# ╠═0c957459-d62c-4816-baf3-d5b7e45d8621
# ╠═37c737d2-0ea7-4dff-ba25-f0bde0cc66fd
# ╠═c345cd74-6678-4f88-ad3f-e3e802e415b6
# ╠═a7e920ae-1cc8-4957-9ae1-8d2107b5fe79
# ╠═e1aacc9f-6870-4dc6-9b5f-c3fe4bef7095
# ╠═d4a7905a-8afc-4ee4-8953-bfbd675c3a36
# ╠═223f5994-4681-4182-8802-b571cc2d7c7f
# ╠═57fe55aa-3fd1-4ae9-b95b-24050af6e593
# ╟─486405b4-6715-42ff-bbaa-adee8fbc2cf6
# ╟─e2f8a679-1514-4aaa-a02c-2c45f28f5066
# ╟─73eb930f-d8d4-4c0a-bced-24463139d1f3
# ╟─d3b15d47-380b-47d9-98b0-bad0a6636bd5
# ╟─ae03db7b-f1d3-4f3b-9297-787cb8b3dcff
# ╟─653f7651-ed88-43ba-a18e-dbd3561e4edf
# ╟─802e98da-35c1-4363-92bb-06b7878aedf5
# ╟─c7057229-f0df-4988-b2e1-e665c1b925ef
# ╟─0b6cc374-1131-475e-a0da-9a9862f795df
# ╠═e1d6f54f-e982-459c-814b-239382ea9885
# ╠═3432e497-037e-4fbe-b5ec-6eab0e51bbf7
# ╠═a5db8e95-a16c-44d2-b489-de67edde07f9
# ╠═1e362a43-7544-4b9a-9bc9-e3f5a7d3d9bd
# ╠═3938b3c2-5a95-40ba-9af9-e46299ca88bd
# ╠═c544d93a-735a-4ed2-a831-57eae3fd7b6b
# ╠═e57911ef-94fc-44ba-99d1-d2c9205f7a3e
# ╠═91a83b0f-675c-4c37-8d6c-12bcb73be459
# ╟─d81e65a0-ab51-4230-8f08-9c5f438c52c7
# ╟─f28ded18-f411-4a9e-8a3b-7893305d7456
# ╟─b86a6a50-70ca-4a48-b8bb-35409bf5c7fe
# ╟─fbac4c2c-eee4-4e0b-a758-f31afb4d203d
# ╟─90ea3a5c-69ee-4d61-a82e-9b5e16431224
# ╟─7464554b-7efc-41a0-b6f5-75fa94b3498b
# ╟─bec4aee7-6be4-4520-933e-853c97f94a1b
# ╟─4a448fcb-45e2-4311-a54b-c0b1db31ac8d
# ╠═5bdaf528-518b-4d52-b7ed-b9bfa170b6bd
# ╠═c4b0f227-6c95-4176-82a5-4358c461a861
# ╠═d7dd42d5-9920-4908-ada4-0294fc1635e7
# ╟─30002726-91c2-4867-93c3-eb7c61d898a6
# ╟─6fa13a2f-cfcd-4175-8a7c-3cd567cfb4a7
# ╟─95ce86af-8d24-4155-a385-34f017d2c841
# ╟─a5b09778-a1ad-4765-a112-6b277f752265
# ╠═a2d8de84-6577-43e6-b9d3-22f08af4a9b1
# ╠═1b8694da-3cf8-49a4-8206-8129fa7e5b83
# ╠═0588d273-7a6a-46a0-b0e0-78c13f8fe688
# ╟─8b64d9d1-8bac-4a9a-bfca-0d7d9c8b02eb
# ╠═02cef2c8-1708-413e-b9df-ab6a9c15aacf
# ╟─4611bd79-684b-4084-8db4-99f6b7f18b9d
# ╟─2dc02479-da9e-4825-9e79-06eeafeb33ff
# ╠═13a7c180-03f5-4e6d-ba22-30f3279b57a2
# ╠═1630ffce-a1ce-4f4c-9671-1c5269c2a945
# ╠═43ae126c-138d-4944-a3c4-307793572ebf
# ╠═d975633d-7b1f-444f-90c4-89c1eec0cd93
# ╟─7f2a7dcf-3658-4228-a49f-b4e267a3a5ab
# ╟─a0d28c87-76c1-4ebf-8b2f-f533763059e5
# ╠═76e4ffb3-e084-4bcf-aca3-61f73b615c6e
# ╠═303c31f7-a3d2-4286-888c-9c0678810581
# ╠═72dbc711-7c9b-4434-b672-ad42e402e23c
# ╠═f642f5bf-d81c-46ed-a941-17c1ab104e8b
# ╟─48d42242-6be5-4ce1-bed8-e31a699f6e32
# ╟─d7c6448e-88e4-4296-a7fc-6422a99fe4b0
# ╠═11dc37d1-3e99-47fa-931f-8acef83c4d7a
# ╠═8edb6011-2655-496a-bb06-082e842b535c
# ╠═ade3e711-547e-42a4-b5ba-f089025752b5
# ╟─a7f2e529-8a96-4042-893d-5f7ee3158697
# ╟─f5a1f575-06e7-4b94-bd7c-451e3f743023
# ╟─b6419e4b-652b-4134-8937-d76ff5d90795
# ╟─ffe768b3-28bd-40d7-b112-b1b488c6c557
# ╟─ef57fa7c-e953-4c52-ac1d-bee152414b66
# ╟─5aedc763-0808-49a6-a6d5-3ade37390400
# ╠═ae356ba7-3182-4cbc-b671-7c5ef9a20433
# ╠═fbfd7d67-40da-455f-b091-47cb7fb6fda8
# ╠═e73c62aa-1937-4771-984f-f5125ffd4a28
# ╟─53f9620a-1e7a-4c66-9f7e-5256bb6d023b
# ╟─94d1beda-944c-4d6a-9538-a0169c863a1b
# ╠═19c1324f-d0ee-48ad-ab1a-23ebaa38f27a
# ╠═a949170a-f7f3-4c5e-8b1b-55909353ee6a
# ╟─20b25bc6-aa15-4432-ba15-3120d0b09f40
# ╟─66e768ba-3dc9-4127-913e-ccea0a2d2fea
# ╟─7be786d5-8dc7-4813-8493-775ff012bc17
# ╟─a028e3bf-6076-4c52-a4ef-71227e9873b4
# ╟─e75e437c-e885-47e3-b24c-3a5118f9d014
# ╟─9bd3af5e-79ba-4956-a140-d0be852489f1
# ╟─77c3f2bc-ce4f-4173-b8b1-bace74bcc6ad
# ╠═fbdfb92f-709e-40b7-ba0b-5b50d46f57c3
# ╠═1e9a79f9-6e58-4e1d-8f97-56f74d971f4e
# ╠═b5f1ae37-7d99-42aa-93d8-157a9e79a81b
# ╟─a73d0b8c-670b-4b0b-aa79-bca1f458d039
# ╟─4c2f7a61-ec74-48cd-9bb8-1d91e449c7ce
# ╠═f01f0420-fb3f-11ee-1599-6def20b9d2d3
# ╠═5ee6970a-dbad-4e6f-8db1-0e982186a11a
# ╟─a790daa2-c97b-4bb3-9009-1a45026ddd5e
# ╠═73cc066c-1576-426c-ab16-1b255b25929c
# ╠═ee81d813-b6dc-42a5-bc13-6dc9a57f510c
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
